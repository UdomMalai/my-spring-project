Create Table tbl_categories 
(
	cateid int primary key auto_increment,
	catename varchar not null,
	catedesc varchar null,
);
Create Table tbl_articles
(
	art_id int primary key auto_increment,
	art_title varchar not null,
	cateid int references tbl_categories(cateid),
	art_desc varchar,
	art_author varchar,
	art_img varchar,
	art_date varchar
);
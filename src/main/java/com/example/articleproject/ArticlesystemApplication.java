package com.example.articleproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArticlesystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArticlesystemApplication.class, args);
	}
}

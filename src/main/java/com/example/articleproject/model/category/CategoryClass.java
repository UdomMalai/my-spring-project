package com.example.articleproject.model.category;

public class CategoryClass {
	private int cateid;
	private String catename;
	private String catedesc;
	public int getCateid() {
		return cateid;
	}
	public void setCateid(int cateid) {
		this.cateid = cateid;
	}
	public String getCatename() {
		return catename;
	}
	public void setCatename(String catename) {
		this.catename = catename;
	}
	public String getCatedesc() {
		return catedesc;
	}
	public void setCatedesc(String catedesc) {
		this.catedesc = catedesc;
	}
	public CategoryClass(int cateid, String catename, String catedesc) {
		super();
		this.cateid = cateid;
		this.catename = catename;
		this.catedesc = catedesc;
	}
	public CategoryClass() {
		super();
	}
	@Override
	public String toString() {
		return "CategoryClass [cateid=" + cateid + ", catename=" + catename + ", catedesc=" + catedesc + "]";
	}
	
}

package com.example.articleproject.model.article;

import com.example.articleproject.model.category.CategoryClass;

public class ArticleClass {
	private int art_id;
	private String art_title;
	private CategoryClass category;
	private String art_desc;
	private String art_author;
	private String art_img;
	private String art_date;
	public int getArt_id() {
		return art_id;
	}
	public void setArt_id(int art_id) {
		this.art_id = art_id;
	}
	public String getArt_title() {
		return art_title;
	}
	public void setArt_title(String art_title) {
		this.art_title = art_title;
	}
	public CategoryClass getCategory() {
		return category;
	}
	public void setCategory(CategoryClass category) {
		this.category = category;
	}
	public String getArt_desc() {
		return art_desc;
	}
	public void setArt_desc(String art_desc) {
		this.art_desc = art_desc;
	}
	public String getArt_author() {
		return art_author;
	}
	public void setArt_author(String art_author) {
		this.art_author = art_author;
	}
	public String getArt_img() {
		return art_img;
	}
	public void setArt_img(String art_img) {
		this.art_img = art_img;
	}
	public String getArt_date() {
		return art_date;
	}
	public void setArt_date(String art_date) {
		this.art_date = art_date;
	}
	public ArticleClass(int art_id, String art_title, CategoryClass category, String art_desc, String art_author,
			String art_img,String art_date) {
		this.art_id = art_id;
		this.art_title = art_title;
		this.category = category;
		this.art_desc = art_desc;
		this.art_author = art_author;
		this.art_img = art_img;
		this.art_date = art_date;

	}
	public ArticleClass() {
		super();
	}
	@Override
	public String toString() {
		return "ArticleClass [art_id=" + art_id + ", art_title=" + art_title + ", category=" + category + ", art_desc="
				+ art_desc + ", art_author=" + art_author + ", art_img=" + art_img + ", art_date=" + art_date + "]";
	}
	
}

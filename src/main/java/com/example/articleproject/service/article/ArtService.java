package com.example.articleproject.service.article;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.articleproject.model.article.ArticleClass;
import com.example.articleproject.repository.article.ArtRepository;

@Service
public class ArtService implements ArtInterface{

	@Autowired
	private ArtRepository artRepository;
	
	@Override
	public void insert(ArticleClass article) {
		artRepository.artInsert(article);
	}

	@Override
	public ArticleClass findOne(int id) {
		return artRepository.artFindOne(id);
	}

	@Override
	public List<ArticleClass> findAll() {
		return artRepository.artAll();
	}

	@Override
	public void delete(int id) {
		artRepository.artDelete(id);
	}

	@Override
	public void update(ArticleClass articleClass) {
		artRepository.artUpdate(articleClass);
	}

}

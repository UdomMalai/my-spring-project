package com.example.articleproject.service.article;

import java.util.List;

import com.example.articleproject.model.article.ArticleClass;

public interface ArtInterface {
	void insert(ArticleClass article);
	ArticleClass findOne(int id);
	List<ArticleClass> findAll();
	void delete(int id);
	void update(ArticleClass articleClass);
}

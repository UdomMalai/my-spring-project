package com.example.articleproject.service.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.articleproject.model.category.CategoryClass;
import com.example.articleproject.repository.category.CateRepository;

@Service
public class CateService implements CateInterface{
	@Autowired
	private CateRepository cateRepository;

	@Override
	public CategoryClass findOneCate(int id) {
		return cateRepository.cateFindOne(id);
	}

	@Override
	public List<CategoryClass> findAllCategory() {
		return cateRepository.cateAll();
	}

	@Override
	public void deleteCate(int id) {
		cateRepository.cateDelete(id);
	}

	@Override
	public void updateCate(CategoryClass categories) {
		cateRepository.cateUpdate(categories);
	}

	@Override
	public void insertCate(CategoryClass categories) {	
		cateRepository.cateInsert(categories);
	}

}

package com.example.articleproject.service.category;

import java.util.List;

import com.example.articleproject.model.category.CategoryClass;

public interface CateInterface {
	CategoryClass findOneCate(int id);
	List<CategoryClass> findAllCategory();
	void deleteCate(int id);
	void updateCate(CategoryClass categories);
	void insertCate(CategoryClass categories);
}

package com.example.articleproject.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class DatabaseConfig {
	@Bean
	@Profile("postgres")
	public DataSource dataSource() {
		DriverManagerDataSource dmds = new DriverManagerDataSource();
		dmds.setDriverClassName("org.postgresql.Driver");
		dmds.setUrl("jdbc:postgresql://127.0.0.1:5432/article_management");
		dmds.setUsername("postgres");
		dmds.setPassword("Udommalai12345");
		return dmds;
	}
	
	@Bean
	@Profile("h2")
	public DataSource hDataSource() {
		EmbeddedDatabaseBuilder embd = new EmbeddedDatabaseBuilder();
		embd.setType(EmbeddedDatabaseType.H2);
		embd.addScripts("sql/table.sql","sql/data.sql");
		return embd.build();		
	}
}

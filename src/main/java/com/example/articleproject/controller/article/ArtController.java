package com.example.articleproject.controller.article;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.articleproject.model.article.ArticleClass;
import com.example.articleproject.model.category.CategoryClass;
import com.example.articleproject.service.article.ArtService;
import com.example.articleproject.service.category.CateService;

@Controller
public class ArtController {
	
	@Autowired
	private ArtService artService;
	@Autowired
	private CateService cateService;
	
	@GetMapping("/article?lang=kh")
	public String khLang() {
		return "article";
	}
	@GetMapping("/article?lang=en")
	public String enLang() {
		return "article";
	}
	@GetMapping("/article")
	public String artical(ModelMap model) {
		List<CategoryClass> categoryClass = cateService.findAllCategory();
		List<ArticleClass> articleClass = artService.findAll();
		model.addAttribute("categories",categoryClass);
		model.addAttribute("articles", articleClass);
		return "article";
	}
	@GetMapping("/article/{id}")
	public String articalOne(@PathVariable("id")int id, ModelMap model) {
		model.addAttribute("category",cateService.findOneCate(id));
		model.addAttribute("article", artService.findOne(id));
		return "display";
	}
	@GetMapping("/delete/{id}")
	public String deleteArticle(@PathVariable("id")int id, ModelMap model) {
		artService.delete(id);
		return "redirect:/article";
	}
	@GetMapping("/edit/{id}")
	public String displayOne(@PathVariable("id")int id, ModelMap model) {
		model.addAttribute("article", artService.findOne(id));
		model.addAttribute("categories",cateService.findAllCategory());
		return "update";
	}
	@GetMapping("/add")
	public String addNewArticle(Model model) {
		model.addAttribute("article",new ArticleClass());
		model.addAttribute("categories",cateService.findAllCategory());
		return "addnew";
	}
	@PostMapping("/add")
	public String saveArticle(@RequestParam("aa") MultipartFile file,@ModelAttribute ArticleClass articleClass,BindingResult result,Model model) {
		String servPaths = "H:/workspace-sts-3.9.3.RELEASE/SpringBootTest/src/main/resources/static/image/";
		String fileName = UUID.randomUUID().toString() + file.getOriginalFilename();
		if(result.hasErrors() || file.isEmpty()) {
			model.addAttribute("categories",cateService.findAllCategory());
			model.addAttribute("article", new ArticleClass());
			return "addnew";
		}
		if (!file.isEmpty()) {
			try {
				Files.copy(file.getInputStream(), Paths.get(servPaths,fileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		articleClass.setCategory(cateService.findOneCate(articleClass.getCategory().getCateid()));
		articleClass.setArt_img("image/" + fileName);
		articleClass.setArt_date(new Date().toString());
		artService.insert(articleClass);
		return "redirect:/article";
	}
	@PostMapping("/edit")
	public String updateArticle(@RequestParam("aa") MultipartFile file,@ModelAttribute ArticleClass articleClass,BindingResult result,Model model) {	
		String servPaths = "H:/workspace-sts-3.9.3.RELEASE/SpringBootTest/src/main/resources/static/image/";
		String fileName = UUID.randomUUID().toString() + file.getOriginalFilename();
		if(result.hasErrors() || file.isEmpty()) {
			model.addAttribute("categories",cateService.findAllCategory());
			model.addAttribute("article", new ArticleClass());
			return "update";
		}
		if (!file.isEmpty()) {
			try {
				Files.copy(file.getInputStream(), Paths.get(servPaths,fileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		articleClass.setCategory(cateService.findOneCate(articleClass.getCategory().getCateid()));
		articleClass.setArt_img("image/" + fileName);
		articleClass.setArt_date(new Date().toString());
		artService.update(articleClass);
		return "redirect:/article";
	}
	
	@GetMapping("/category?lang=kh")
	public String khLang1() {
		return "category";
	}
	@GetMapping("/category?lang=en")
	public String enLang1() {
		return "category";
	}
	@GetMapping("/category")
	public String category(ModelMap model) {
		List<CategoryClass> categoryClass = cateService.findAllCategory();
		model.addAttribute("categories",categoryClass);
		return "category";
	}
}

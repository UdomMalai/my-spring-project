package com.example.articleproject.controller.category;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.example.articleproject.model.category.CategoryClass;
import com.example.articleproject.service.category.CateService;

//@Controller
public class CateController {
	@Autowired
	private CateService cateService;
	
	@GetMapping("/category?lang=kh")
	public String khLang() {
		return "category";
	}
	@GetMapping("/category?lang=en")
	public String enLang() {
		return "category";
	}
	@GetMapping("/category")
	public String category(ModelMap model) {
		List<CategoryClass> categoryClass = cateService.findAllCategory();
		model.addAttribute("categories",categoryClass);
		return "category";
	}
	@GetMapping("/delete/{id}")
	public String deleteCategory(@PathVariable("id")int id, ModelMap model) {
		cateService.deleteCate(id);
		return "redirect:/category";
	}
	@GetMapping("/edit/{id}")
	public String displayOne(@PathVariable("id")int id, ModelMap model) {
		model.addAttribute("article", cateService.findOneCate(id));
		return "update";
	}
}

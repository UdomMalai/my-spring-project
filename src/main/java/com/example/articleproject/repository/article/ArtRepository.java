package com.example.articleproject.repository.article;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.example.articleproject.model.article.ArticleClass;

@Repository
public interface ArtRepository {
	@Insert("INSERT INTO public.tbl_articles(\r\n" + 
			"	art_title, cateid, art_desc, art_author, art_img, art_date)\r\n" + 
			"	VALUES (#{art_title}, #{category.cateid}, #{art_desc}, #{art_author}, #{art_img}, #{art_date});")
	public boolean artInsert(ArticleClass articleClass);
	@Update("UPDATE public.tbl_articles\r\n" + 
			"	SET art_title=#{art_title}, cateid=#{category.cateid}, art_desc=#{art_desc}, art_author=#{art_author}, art_img=#{art_img}, art_date=#{art_date}\r\n" + 
			"	WHERE art_id = #{art_id};")
	public boolean artUpdate(ArticleClass articleClass);
	@Delete("Delete from tbl_articles where art_id= #{id}")
	public boolean artDelete(int artid);
	@Select("Select art_id,art_title,tbl_articles.cateid,catename,art_desc,art_author,art_img,art_date from tbl_articles inner join tbl_categories on tbl_articles.cateid = tbl_categories.cateid where art_id = #{id}")
	@Results({
		@Result(property="art_id",column="art_id"),
		@Result(property="art_title",column="art_title"),
		@Result(property="category.cateid",column="cateid"),
		@Result(property="category.catename",column="catename"),
		@Result(property="art_desc",column="art_desc"),
		@Result(property="art_author",column="art_author"),
		@Result(property="art_img",column="art_img"),
		@Result(property="art_date",column="art_date")
	})
	public ArticleClass artFindOne(int artid);
	@Select("Select art_id,art_title,tbl_articles.cateid,catename,art_desc,art_author,art_img,art_date from tbl_articles inner join tbl_categories on tbl_articles.cateid = tbl_categories.cateid order by art_id asc")
	@Results({
		@Result(property="art_id",column="art_id"),
		@Result(property="art_title",column="art_title"),
		@Result(property="category.cateid",column="cateid"),
		@Result(property="category.catename",column="catename"),
		@Result(property="art_desc",column="art_desc"),
		@Result(property="art_author",column="art_author"),
		@Result(property="art_img",column="art_img"),
		@Result(property="art_date",column="art_date")
	})
	public List<ArticleClass> artAll();
}

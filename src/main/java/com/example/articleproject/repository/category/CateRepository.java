package com.example.articleproject.repository.category;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.example.articleproject.model.category.CategoryClass;

@Repository
public interface CateRepository {
	@Insert("Insert into tbl_categories (catename,catedesc) Values(#{catename},#{catedesc})")
	public boolean cateInsert(CategoryClass categoryClass);
	@Update("Update tbl_categories set catename = #{catename}, catedesc = #{catedesc} where cateid=#{id}")
	public boolean cateUpdate(CategoryClass categoryClass);
	@Delete("Delete from tbl_categories where cateid= #{id}")
	public boolean cateDelete(int cateid);
	@Select("Select cateid,catename,catedesc from tbl_categories where cateid= #{id}")
	@Results({
		@Result(property="cateid",column="cateid"),
		@Result(property="catename",column="catename"),
		@Result(property="catedesc",column="catedesc"),
	})
	public CategoryClass cateFindOne(int cateid);
	@Select("Select cateid,catename,catedesc from tbl_categories")
	@Results({
		@Result(property="cateid",column="cateid"),
		@Result(property="catename",column="catename"),
		@Result(property="catedesc",column="catedesc"),
	})
	public List<CategoryClass> cateAll();
}
